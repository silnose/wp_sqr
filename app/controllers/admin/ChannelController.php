<?php

class ChannelController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
            	//
            $channels = Channel::paginate();
            
            return View::make('admin/channels/list')->with('channels', $channels);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
             return View::make('admin/channels/create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
            
                
              $rules = array(
            'ch_name' => 'required | unique:channels,ch_name',
            
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('channels/create')
                            ->withErrors($validator)->withInput(Input::all());
        } 
        
        
            $channel= new Channel;
        $channel->ch_name = ucwords(strtolower(Input::get('ch_name')));
        $channel->save();
        Session::flash('message', '¡Canal Creado Con Exito!');
        return Redirect::to('channels');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
            
            $channel = Channel::find($id);
        
            


        return View::make('admin/channels/show')->with('channel',$channel);
        }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
            $channel = Channel::find($id);
           
            return View::make('admin/channels/edit')->with('channel',$channel);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
            
                
              $rules = array(
            'ch_name' => 'required',
            
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('channels/edit/'.$id)
                            ->withErrors($validator)->withInput(Input::all());
        } 
        
        
            $channel = Channel::find($id);
        
            $channel->ch_name = ucwords(strtolower(Input::get('ch_name')));
            $channel->save();
            Session::flash('message', '¡Canal Editado Con Exito!');
        return Redirect::to('channels');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
            $channel = Channel::find($id);
        $channel->delete();
        Session::flash('message', '¡Canal Eliminado Con Exito!');
        return Redirect::to('channels');
         
	}


}
