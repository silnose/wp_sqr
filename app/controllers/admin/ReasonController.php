<?php

class ReasonController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
        $reasons = Reason::paginate();
        return View::make('admin/reasons/list')->with('reasons', $reasons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
        $departments = Department::lists('d_name', 'd_id');

        return View::make('admin/reasons/create')->with('departments', $departments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
        $rules = array(
            'r_name' => 'required | unique:reasons,r_name',
            'r_term' => 'integer | required '
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('reasons/create')
                            ->withErrors($validator)->withInput(Input::all());
        }


        $reason = new Reason;

        $reason->r_name = ucwords(strtolower(Input::get('r_name')));
        $reason->r_detail = Input::get('r_detail');
        $reason->r_term = Input::get('r_term');
        $reason->r_department_id = Input::get('r_department_id');

        $reason->save();
        Session::flash('message', '¡Motivo Creado Con Exito!');
        return Redirect::to('reasons');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
        $reason = Reason::find($id);




        $department = Reason::find($id)->department;





//$department contiene el registro al cual el motivo 2 pertence.

  

        return View::make('admin/reasons/show', ['reason'=>$reason , 'department'=>$department]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //

        $reason = Reason::find($id);


        $data = [
            'reason' => $reason,
            'department_id' => Department::lists('d_name', 'd_id'),
        ];

        return View::make('admin/reasons/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
        $rules = array(
            'r_name' => 'required ',
            'r_term' => 'integer | required '
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('reasons/edit/'. $id)
                            ->withErrors($validator)->withInput(Input::all());
        }




        $reason = Reason::find($id);
        $reason->r_name = ucwords(strtolower(Input::get('r_name')));
        $reason->r_detail = Input::get('r_detail');
        $reason->r_term = Input::get('r_term');
        $reason->r_department_id = Input::get('r_department_id');

        $reason->save();
        Session::flash('message', '¡Motivo Editado Con Exito!');
        return Redirect::to('reasons');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //

        $reason = Reason::find($id);
        $reason->delete();
        //$reason->user()->detach(5);
        Session::flash('message', '¡Motivo Eliminado Con Exito!');
        return Redirect::to('reasons');
    }

}
