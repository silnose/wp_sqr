<?php

class ComplaintController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
        //$complaints = Complaint::all();

        if (Auth::user()->u_type == 'Manager'):
            $complaints = DB::table('complaints')
                            ->join('reasons', 'complaints.c_reason_id', '=', 'reasons.r_id')
                            ->join('reason_user', 'reasons.r_id', '=', 'reason_user.ru_reason_id')
                            ->where('reason_user.ru_user_id', '=', Auth::user()->u_id)
                            ->select('complaints.c_id', 'complaints.c_name', 'complaints.c_lastname', 'complaints.c_term', 'complaints.c_state', 'reasons.r_term', 'reasons.r_name')
                            ->groupBy('complaints.created_at', 'desc')->paginate(10);
        elseif (Auth::user()->u_type == 'Administrador'):
            $complaints = Complaint::paginate();
            $complaints = DB::table('complaints')
                    ->join('reasons', 'complaints.c_reason_id', '=', 'reasons.r_id')
                    ->join('departments', 'reasons.r_department_id', '=', 'departments.d_id')
                    ->select('complaints.c_id', 'complaints.c_name', 'complaints.c_lastname', 'complaints.c_term', 'complaints.c_state', 'complaints.c_final_date', 'reasons.r_term', 'reasons.r_name','departments.d_name' )
                    ->orderBy('complaints.c_final_date', 'asc')
                    ->orderBy('complaints.c_state', 'desc')
                    ->paginate(10);

        endif;



        return View::make('admin/complaints/list')->with('complaints', $complaints);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function search() {

        $q = Input::get('q');
        $complaints = DB::table('complaints')
                ->join('reasons', 'complaints.c_reason_id', '=', 'reasons.r_id')
                ->join('departments', 'reasons.r_department_id', '=', 'departments.d_id')
                ->select('complaints.c_id', 'complaints.c_name', 'complaints.c_lastname', 'complaints.c_state', 'complaints.c_term', 'complaints.c_final_date', 'reasons.r_name', 'departments.d_name')
                ->where('c_dni', $q)
               ->orwhere( 'c_lastname', 'LIKE', '%'.$q.'%')->paginate(10);
               
                

     

    
        return View::make('admin/complaints/list')->with('complaints', $complaints);
    }

    public function json_client() {

        $dni = Input::get('c_dni');

        $client = DB::table('complaints')
                        ->select('complaints.c_id', 'complaints.c_name', 'complaints.c_lastname', 'complaints.c_phone', 'complaints.c_cellphone', 'complaints.c_work', 'complaints.c_workphone')
                        ->where('c_dni', $dni)
                        ->groupBy('complaints.c_id')
                        ->orderBy('complaints.c_id', 'desc')->first();

        //echo json_encode($client);



        echo json_encode($client);
    }

    public function create() {
        //

        $reasons = Reason::all();

        $data = [
            'reasons' => $reasons,
            'channels' => Channel::lists('ch_name', 'ch_id'),
            'js' => array('assets/js/complaints/create.js')
        ];


        return View::make('admin/complaints/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        //

        $rules = array(
            'c_name' => 'required  ',
            'c_lastname' => 'required ',
            'c_dni' => 'required | numeric ',
            'c_email' => 'email',
            'c_detail' => 'required '
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('complaints/create')
                            ->withErrors($validator)->withInput(Input::all());
        }

        $complaint = new Complaint;
        $complaint->c_type = Input::get('c_type');
        $complaint->c_name = ucwords(strtolower(Input::get('c_name')));
        $complaint->c_lastname = ucwords(strtolower(Input::get('c_lastname')));
        $complaint->c_dni = ucwords(strtolower(Input::get('c_dni')));
        $complaint->c_phone = ucwords(strtolower(Input::get('c_phone')));
        $complaint->c_cellphone = ucwords(strtolower(Input::get('c_cellphone')));
        $complaint->c_email = Input::get('c_email');
        $complaint->c_work = ucwords(strtolower(Input::get('c_work')));
        $complaint->c_workphone = ucwords(strtolower(Input::get('c_workphone')));
        $complaint->c_detail = Input::get('c_detail');
        $complaint->c_state = Input::get('c_state');
        $complaint->c_doc = Input::get('c_doc');
        $complaint->c_channel_id = Input::get('c_channel_id');
        $complaint->c_user_id = Input::get('c_user_id');
        $complaint->c_reason_id = Input::get('c_reason_id');


        $reasons = Reason::find(Input::get('c_reason_id'));
        $complaint->c_term = $reasons->r_term;

        $fecha = date('Y-m-j');
        $nuevafecha = strtotime('+' . $complaint->c_term . 'day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $complaint->c_final_date = $nuevafecha;


        $associated_itens = DB::table('reason_user')->where('ru_reason_id', '=', $complaint->c_reason_id)->lists('ru_user_id');
        //->select('ru_user_id')->get();



        foreach ($associated_itens as $var) {

            $user = User::find($var);
            $para = $user->u_email;

            // Varios destinatarios
            //$para = 'murgosilvana@gmail.com';
//$para  = 'gudu.chango@gmail.com';
// título
            $título = 'Nuevo Queja- Reclamo - Sugerencia';

// mensaje
            $mensaje = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" =
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <meta http-equiv="Content-type" content="text/html; 
              charset=UTF-8;" />
    </head>

    <body bgcolor="#FFFFFF" text="#000000" link="#0066CC" =
          style="margin:0; padding:0; background-color:#FFF;">
          <div
            style="font-family:arial,sans-serif;font-size:13px;width:620px;">
           
            <p style="border-top:1px solid #D8D8D8; margin:0; padding:5px; 
               background-color:#F0F0F0;">
               Nueva Queja, Reclamo o Sugerencia:<br />
                <p style="padding: 0pt 5px;">
                <strong>Cliente: </strong>
                ' . $complaint->c_name . ' -' . $complaint->c_lastname . '<br />
                <br />
                Por favor ingrese a la web para conocer en detalle
            </p>
                <br />
            </p>
            <br />
        </div>   
    </body>
</html>
';

// Para enviar un correo HTML, debe establecerse la cabecera Content-type
            $cabeceras = 'MIME-Version: 1.0' . "\r\n";
            $cabeceras .= 'Content-type: text/html; charset=utf8' . "\r\n";

// Cabeceras adicionales
//$cabeceras .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
//$cabeceras .= 'From: '.$_POST['name'].' <'.$_POST['email'].'>' . "\r\n";
//$cabeceras .= 'Cc: birthdayarchive@example.com' . "\r\n";
//$cabeceras .= 'Bcc: birthdaycheck@example.com' . "\r\n";
//$cabeceras .= 'Reply-To:'.$_POST['email'];
// Enviarlo
            mail($para, $título, $mensaje, $cabeceras);
        }




        $complaint->save();



        Session::flash('message', '¡Reclamo Creado Con Exito!');

        return Redirect::to('complaints');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    protected function dias_restantes($fecha_final) {
        $fecha_actual = date("Y-m-d");
        $s = strtotime($fecha_final) - strtotime($fecha_actual);
        $d = intval($s / 86400);
        $diferencia = $d;
        return $diferencia;
    }

    public function show($id) {
        //
        

        $complaint = Complaint::find($id);
        
        $reason = Complaint::find($id)->reason;
        $department = Reason::find($reason->r_department_id)->department;
        $reason_day = $complaint->reason()->first()->term;

        //Dias que quedan para la resolucion
        $fecha_actual = date("Y-m-d");
        $s = strtotime($complaint->c_final_date) - strtotime($fecha_actual);
        $dias = intval($s / 86400);
        $channel = $complaint->channel()->first()->name;

        return View::make('admin/complaints/show', ['department' => $department, 'channel' => $channel, 'dias' => $dias, 'complaint' => $complaint, 'reason' => $reason, 'reason_day' => $reason_day]);
    }

    public function pdf($id) {
        //
        $complaint = Complaint::find($id);
        $reason = Complaint::find($id)->reason;
        $department = Reason::find($reason->r_department_id)->department;

//PDF
        $html = View::make('admin/complaints/pdf', ['department' => $department, 'complaint' => $complaint, 'reason' => $reason]);
        setlocale(LC_NUMERIC, "C");
        $headers = array('Content-Type' => 'application/pdf');

        return Response::make(PDF::load($html, 'A4', 'portrait')->show('my_pdf'), 200, $headers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
        $complaint = Complaint::find($id);
        $data = [
            'complaint' => $complaint,
            'reason_id' => Reason::lists('r_name', 'r_id'),
            'channels' => Channel::lists('ch_name', 'ch_id'),
        ];

        return View::make('admin/complaints/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
        $rules = array(
            'c_name' => 'required  ',
            'c_lastname' => 'required  ',
            'c_dni' => 'required | numeric ',
            'c_email' => 'email',
            'c_detail' => 'required '
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('complaints/edit/' . $id)
                            ->withErrors($validator)->withInput(Input::all());
        }

        $complaint = Complaint::find($id);
        $complaint->c_type = Input::get('c_type');
        $complaint->c_name = ucwords(strtolower(Input::get('c_name')));
        $complaint->c_lastname = ucwords(strtolower(Input::get('c_lastname')));
        $complaint->c_dni = ucwords(strtolower(Input::get('c_dni')));
        $complaint->c_phone = ucwords(strtolower(Input::get('c_phone')));
        $complaint->c_cellphone = ucwords(strtolower(Input::get('c_cellphone')));
        $complaint->c_email = ucwords(strtolower(Input::get('c_email')));
        $complaint->c_work = ucwords(strtolower(Input::get('c_work')));
        $complaint->c_workphone = ucwords(strtolower(Input::get('c_workphone')));
        $complaint->c_detail = Input::get('c_detail');
        $complaint->c_state = Input::get('c_state');
        $complaint->c_doc = Input::get('c_doc');
        $complaint->c_channel_id = Input::get('c_channel_id');
        $complaint->c_user_id = Input::get('c_user_id');
        $complaint->c_reason_id = Input::get('c_reason_id');

        if ($complaint->c_term != Input::get('c_term')) {

            $fecha = date('Y-m-j');
            $nuevafecha = strtotime('+' . $complaint->c_term . 'day', strtotime($fecha));
            $nuevafecha = date('Y-m-j', $nuevafecha);

            $complaint->c_final_date = $nuevafecha;
        }


       /*
          $reasons = Reason::find(Input::get('c_reason_id'));
          $complaint->c_term = $reasons->r_term;

          $fecha = date('Y-m-j');
          $nuevafecha = strtotime('+' . $complaint->c_term . 'day', strtotime($fecha));
          $nuevafecha = date('Y-m-j', $nuevafecha);

          $complaint->c_final_date = $nuevafecha;
        * */
        
       
        $complaint->save();
        Session::flash('message', '¡Reclamo Editado Con Exito!');

        return Redirect::to('complaints');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $complaint = Complaint::find($id);
        $complaint->delete();
        Session::flash('message', '¡Reclamo Eliminado Con Exito!');
        return Redirect::to('complaints');
    }

}
