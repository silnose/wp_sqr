<?php

class DepartmentController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
        $departments = Department::paginate();

        return View::make('admin/departments/list')->with('departments', $departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
        return View::make('admin/departments/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //


        $rules = array(
            'd_name' => 'required  | unique:departments,d_name',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('departments/create')
                            ->withErrors($validator)->withInput(Input::all());
        }


        $department = new Department;
        $department->d_name = ucwords(strtolower(Input::get('d_name')));
        $department->save();

        Session::flash('message', '¡Departamento Creado Con Exito!');
        return Redirect::to('departments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //

        $department = Department::find($id);





        return View::make('admin/departments/show')->with('department', $department);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //





        $department = Department::find($id);

        return View::make('admin/departments/edit')->with('department', $department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //

        $rules = array(
            'd_name' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('departments/edit/' . $id)
                            ->withErrors($validator)->withInput(Input::all());
        }

        $department = Department::find($id);

        $department->d_name = ucwords(strtolower(Input::get('d_name')));
        $department->save();
        Session::flash('message', '¡Departamento Editado Con Exito!');
        return Redirect::to('departments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $department = Department::find($id);
        $department->delete();
        Session::flash('message', '¡Departamento Eliminado Con Exito!');
        return Redirect::to('departments');
    }

}
