<?php

class UserController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $users = User::paginate();
        return View::make('admin/users/list')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
        $data = [
            'reasons' => Reason::all(),
            'departments' => Department::lists('d_name', 'd_id'),
        ];

        return View::make('admin/users/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
        $rules = array(
            'u_name' => 'required | unique:users,u_name',
            'u_nick' => 'required | unique:users,u_nick',
            'u_email' => 'required | email | unique:users,u_email',
            'reason' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('users/create')
                            ->withErrors($validator)->withInput(Input::all());
        }

        $user = new User;
        $user->u_name = ucwords(strtolower(Input::get('u_name')));
        $user->u_nick = strtolower(Input::get('u_nick'));
        $user->u_password = Hash::make(Input::get('u_nick'));
        $user->u_type = ucwords(strtolower(Input::get('u_type')));
        $user->u_email = Input::get('u_email');
        $user->u_department_id = Input::get('u_department_id');

        $user->save();

        $userid = $user->u_id;

        foreach (Input::get('reason') as $var) {

            $reason_user = new Reason_User();
            $reason_user->ru_user_id = $userid;
            $reason_user->ru_reason_id = $var;
            $reason_user->save();
        }
        Session::flash('message', '¡Usuario Creado Con Exito!');
        return Redirect::to('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //

         $user = User::find($id); 
        $reason_user = Reason_User::reason_user_list($id);

        $data = array(
            $reason_user,
            $user
        );
        return View::make('admin/users/show', ['user' => $user, 'reason_user' => $reason_user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //'assigned' => $group->permissions->lists('id')
        // $data = [
        //'reasons' => Reason::all(),
        //'user' => $user,
        //   'departments' => Department::lists('d_name', 'd_id'),
        //'reason' => $user->reason,
        //'reason_user' => $user->$reasons->list('id')
        //];
        //return View::make('admin/users/edit', $data);
        //get all itens
        $user = User::find($id);
        $departments = Department::lists('d_name', 'd_id');
        $reasons = Reason::get();
        //just get the itens associated with profile from the pivot table, returned as an array
        $associated_itens = DB::table('reason_user')->where('ru_user_id', '=', $id)->lists('ru_reason_id');
        return View::make('admin/users/edit', compact('departments', 'user', 'reasons', 'associated_itens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //

        $rules = array(
            'u_name' => 'required ',
            'u_nick' => 'required',
            'u_email' => 'required | email',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('users/edit/' . $id)
                            ->withErrors($validator)->withInput(Input::all());
        }

        $user = User::find($id);
        $user->u_name = ucwords(strtolower(Input::get('u_name')));
        $user->u_nick = strtolower(Input::get('u_nick'));
        $user->u_type = ucwords(strtolower(Input::get('u_type')));
        $user->u_email = Input::get('u_email');
        //$user->u_password = Hash::make(Input::get('u_password'));
        $user->u_department_id = Input::get('u_department_id');
        $user->save();
        $userid = $user->u_id;
        //just get the itens associated with profile from the pivot table, returned as an array
        $associated_itens = DB::table('reason_user')->where('ru_user_id', '=', $user->u_id)->delete();

        foreach (Input::get('reason') as $var) {
            $reason_user = new Reason_User();
            $reason_user->ru_user_id = $userid;
            $reason_user->ru_reason_id = $var;
            $reason_user->save();
        }
        Session::flash('message', '¡Usuario Editado Con Exito!');
        return Redirect::to('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $user = User::find($id);
        $user->delete();
        Session::flash('message', '¡Usuario Eliminado Con Exito!');
        return Redirect::to('users');
    }

}
