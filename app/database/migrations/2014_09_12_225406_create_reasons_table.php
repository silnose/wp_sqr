<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReasonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            
                 Schema::create('reasons', function($table)
            
            {
                      $table->increments('r_id');
                      $table->string('r_name');
                      $table->string('r_detail');
                      $table->integer('r_term');
                      $table->integer('r_department_id');
                      $table->timestamps();
                     
                     
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('reasons');
	}

}
