<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
public function up()
	{
		//
             Schema::create('users', function($table)
            
            {
                      $table->increments('u_id');
                      $table->string('u_name');
                      $table->string('u_nick');
                      $table->string('u_password');
                      $table->string('u_type');
                      $table->string('u_email');
                      $table->integer('u_department_id');
                      $table->timestamps();
                     
                     
            });
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('users');
	}
}
