<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('complaints', function($table)
            
            {
                
                $table->increments('c_id');
                
                $table->string('c_type');
                $table->string('c_name');
                $table->string('c_lastname');
                
                $table->integer('c_dni');
                $table->integer('c_phone');
                $table->integer('c_cellphone');
                
                $table->string('c_work');
                
                $table->integer('c_workphone');
                
                $table->string('c_email');
                $table->string('c_detail');
                $table->string('c_state');
                $table->string('c_doc');
                
                $table->integer('c_channel_id');
                $table->integer('c_user_id');
                $table->integer('c_reason_id');
                $table->integer('c_term');
                $table->date('c_final_date');
                
                
                $table->timestamps();
                
                
                
                
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('complaints');
	}

}
