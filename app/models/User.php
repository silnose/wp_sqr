<?php


use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
class User extends Eloquent implements UserInterface {

    
/**
* The database table used by the model.
*
* @var string
*/
protected $table = 'users';
protected $primaryKey = 'u_id';
/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/


	  public function reason()
    {
        return $this->belongsToMany('Reason', 'reason_user', 'ru_reason_id', 'ru_user_id');
    }
    
    //recibe 4 parametros. El primero es el otro módelo cual el cual se quiere hacer relación, 
    ////el segundo es la tabla que contiene los ids de los dos modelos y hace la unión.
    // El tercer y cuarto parámetro 
    ////son los nombres de los id que Laravel debe buscar en la tabla intermedia para hacer la relación.

    public function getAuthIdentifier() {
        return $this->u_id;
        
    }

    public function getAuthPassword() {
        
        return $this->u_password;
        
    }

    public function getRememberToken() {
        
    }

    public function getRememberTokenName() {
        
    }

    public function getReminderEmail() {
        
    }

    public function setRememberToken($value) {
        
    }

}



