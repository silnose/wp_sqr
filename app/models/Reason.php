<?php


class Reason extends Eloquent{ 

	

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'reasons';
        protected $primaryKey = 'r_id';
	/**
         * 
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	
//relación uno a muchos entre motivos y quejas
        public function complaint()
    {
        return $this->hasMany('Complaint','c_id');
    }
 
       public function allshow() {
     
     
     $sql= 'SELECT * FROM reasons';
     return DB::select($sql);
     
     
 }
 
 
   public function department()
    {
        return $this->belongsTo('Department', 'r_department_id');
        // La relación Pertenece a se declara con la función belongsTo
        // esta acepta dos parámetros
        // El primero es la tabla a donde pertecene la relación
        // El segundo es el id de la tabla padre en la tabla actual
        // En este caso seria el id de Asignatura en tema
    }
    
 
      public function users()
    {
        return $this->belongsToMany("User");
    }    
    
    
}
