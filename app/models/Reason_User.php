<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




class Reason_User extends Eloquent{ 

	

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'reason_user';
        
        
      public static function reason_user_list($id){
            
         $rows = DB::table('reason_user')->join( 'reasons','ru_reason_id','=','r_id' )
                 ->join( 'users','ru_user_id','=','u_id' )
                 ->where('u_id', '=', $id)
                 ->get();

        return $rows;
          
      } 
        
        
}

