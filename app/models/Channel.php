<?php


class Channel extends Eloquent{ 

	

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
       
        protected $table = 'channels';
        protected $primaryKey = 'ch_id';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	
        

           public function complaint(){
        return $this->hasMany('Complaint', 'ch_id');
        /// Para declarar una relación uno a muchos se hace uso de la función hasMany().
        // Al igual que hasOne, esta función recibe dos parámetros.
        // El primero es el modelo al cual se desea asociar
        // El segundo es el id con el que se van a relacionar los modelos.
    }
    
 
 
}
