<?php

class Complaint extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'complaints';
        protected $primaryKey = 'c_id';
        /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
            
            
    
    //un post pertenece a un usuario
    public function reason()
    {
        return $this->belongsTo('Reason','c_reason_id');
    }
	
    
      //un post pertenece a un usuario
    public function channel()
    {
        return $this->belongsTo('Channel', 'c_channel_id');
    }

    
    public function sumar( ){
        
 
         
     $sql= 'select DATE_ADD(NOW(),INTERVAL 1 DAY)';
     return DB::select($sql);
        
    }
}
