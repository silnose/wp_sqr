@extends ('layout')

@section ('title') Canales de Comunicación @stop

@section ('content')

@section ('pageheader') Canales de Comunicación @stop

<a class="btn btn-success" href="{{ URL('/channels/create' ) }}" >Nuevo Canal</a>
<br><br>

<ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('#') }}">Canales</a></li>
</ol>

@if (Session::has('message'))
<div class="alert alert-warning">{{ Session::get('message') }}</div>
@endif


<table class="table table-striped">
    <tr>
        <th>N°</th>
        <th>Canal</th>
        <th>Acciones</th>
    
         
       
    </tr>
    @foreach ($channels as $var)
    <tr>
        <td>{{ $var->ch_id }}</td>
        <td>{{ $var->ch_name }}</td>
      
        <td><a href="{{ URL('/channels/show', $var->ch_id ) }}" title="ver" > <span class="btn btn-success glyphicon glyphicon-eye-open"></span></a>
            <a href="{{ URL('/channels/edit', $var->ch_id ) }}" title="editar"><span class="btn btn-warning glyphicon glyphicon-edit"></span></a>
            <a href="{{ URL('/channels/destroy', $var->ch_id )}}" title="eliminar" ><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
        </td>
        
        
        
    </tr>
    @endforeach
  </table>
  
 {{ $channels->links() }}  


  

@stop