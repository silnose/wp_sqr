@extends ('layout')

@section ('title') Canales de Comunicación @stop

@section ('content')

@section ('pageheader') Editar Canal de Comunicación @stop


<div class="row">
    <div class="row">
    <ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('/channels') }}">Canales</a></li>
    <li><a href="{{ URL::to('#') }}">Editar Canal</a></li>
</ol>
        
    <div class="col-md-12">

        {{ Form::model($channel, array('url' => 'channels/update/'.$channel->ch_id, 'method' => 'PUT', 'class' => 'form-horizontal'))   }}
        <fieldset>


        
   
            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Canal*</label>
                <div class="col-sm-10 ">
                    {{ Form::input('text', 'ch_name', null, ['class' => 'form-control']) }}

                </div>
            </div>
          

            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="reset" class="btn btn-default">Cancelar</button>
                        
                    </div>
                </div>
            </div>

         
            
        </fieldset>
        {{ Form::close() }}
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->











@stop
