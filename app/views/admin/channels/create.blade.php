@extends ('layout')

@section ('title') Canales de Comunicación @stop

@section ('content')

@section ('pageheader') Nuevo Canal de Comunicación  @stop


<div class="row">
    <ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('/channels') }}">Canales</a></li>
    <li><a href="{{ URL::to('#') }}">Nuevo Canal</a></li>
</ol>
    
    <div class="col-md-12">

        
        
        
        {{ Form::open(array('url' => 'channels/store', 'method' => 'POST', 'class' => 'form-horizontal'))   }}
        <fieldset>


       

   
            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Canal:</label>
                <div class="col-sm-10 ">
                    {{ Form::text('ch_name', Input::old('ch_name'), array('class' => 'form-control')) }}
    @if($errors->has('ch_name'))    
    <p class="text-danger">{{ $errors->first('ch_name') }}</p>
    @endif

                </div>
            </div>
          





            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>
                    </div>
                </div>
            </div>

         
            
        </fieldset>
        {{ Form::close() }}
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->











@stop