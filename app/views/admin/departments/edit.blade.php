@extends ('layout')

@section ('title') Departamentos @stop

@section ('content')

@section ('pageheader') Editar Departamento @stop


<div class="row">
    <ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('departments') }}">Departamentos</a></li>
    <li><a href="{{ URL::to('#') }}">Editar Departamento</a></li>

</ol>
    
    <div class="col-md-12">

        {{ Form::model($department, array('url' => 'departments/update/'.$department->d_id, 'method' => 'PUT', 'class' => 'form-horizontal'))   }}
        <fieldset>


        
   
            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Departamento *</label>
                <div class="col-sm-10 ">
                     {{ Form::text('d_name', Input::old('d_name'), array('class' => 'form-control')) }}
    @if($errors->has('d_name'))    
    <p class="text-danger">{{ $errors->first('d_name') }}</p>
    @endif

                </div>
            </div>
          

            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>
                        
                    </div>
                </div>
            </div>

         
            
        </fieldset>
        {{ Form::close() }}
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->











@stop
