@extends ('layout')

@section ('title') Departamentos @stop

@section ('content')

@section ('pageheader') Departamentos @stop

<div class="row">
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Inicio</a></li>
        <li><a href="{{ URL::to('departments') }}">Departamentos</a></li>
        <li><a href="{{ URL::to('#') }}">Ver Departamento</a></li>

    </ol>

    <div class="jumbotron text-center">

        <h2>{{ $department->d_name }} </h2>
    </div>  
</div>       






@stop