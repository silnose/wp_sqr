@extends ('layout')

@section ('title') Departamentos @stop

@section ('content')

@section ('pageheader') Departamentos @stop

<a class="btn btn-success" href="{{ URL('/departments/create' ) }}" >Nuevo Departamento</a>
<br><br>
<ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('#') }}">Departamentos</a></li>
    

</ol>

@if (Session::has('message'))
<div class="alert alert-warning">{{ Session::get('message') }}</div>
@endif


<table class="table table-striped">
    <tr>
        <th>N°</th>
        <th>Departamento</th>
        <th>Acciones</th>
         
       
    </tr>
    @foreach ($departments as $var)
    <tr>
        <td>{{ $var->d_id }}</td>
        <td>{{ $var->d_name }}</td>

           <td><a href="{{ URL('/departments/show', $var->d_id ) }}" title="ver" > <span class="btn btn-success glyphicon glyphicon-eye-open"></span></a>
            <a href="{{ URL('/departments/edit', $var->d_id ) }}" title="editar"><span class="btn btn-warning glyphicon glyphicon-edit"></span></a>
            <a href="{{ URL('/departments/destroy', $var->d_id ) }}" title="eliminar" ><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
        </td>

        
        
    </tr>
    @endforeach
  </table>
  
 {{ $departments->links() }}  


  

@stop