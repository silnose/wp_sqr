@extends ('layout')

@section ('title') Usuarios @stop

@section ('content')

@section ('pageheader') Usuarios @stop

<a class="btn btn-success" href="{{ URL('/users/create' ) }}" >Nuevo Usuario</a>
<br><br>
        <ol class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Inicio</a></li>
        <li><a href="{{ URL::to('#') }}">Usuarios</a></li>
        

    </ol>

@if (Session::has('message'))
<div class="alert alert-warning">{{ Session::get('message') }}</div>
@endif


<table class="table table-striped">
    <tr>
        <th>N°</th>
        <th>Usuario</th>
        <th>Tipo</th>
        <th>Acciones</th>
        



    </tr>
    @foreach ($users as $var)
    <tr>
        <td>{{ $var->u_id }}</td>
        <td>{{ $var->u_name }}</td>
        <td>{{ $var->u_type }}</td>

        <td><a href="{{ URL('/users/show', $var->u_id ) }}" title="ver" > <span class="btn btn-success glyphicon glyphicon-eye-open"></span></a>
            <a href="{{ URL('/users/edit', $var->u_id ) }}" title="editar"> <!--onclick="return false"--> <span class="btn btn-warning glyphicon glyphicon-edit"></span></a>
            <a href="{{ URL('/users/destroy', $var->u_id ) }}" title="eliminar" ><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
        </td>




    </tr>
    @endforeach
</table>

{{ $users->links() }}  




@stop