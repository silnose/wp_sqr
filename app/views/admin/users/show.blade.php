@extends ('layout')

@section ('title') Usuarios @stop

@section ('content')

@section ('pageheader') Usuarios @stop


<div class="row">
        <ol class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Inicio</a></li>
        <li><a href="{{ URL::to('/users') }}">Usuarios</a></li>
        <li><a href="{{ URL::to('#') }}">Ver Usuario</a></li>

    </ol>

<div class="jumbotron text-center">

    <h2>{{ $user->u_name }}</h2>
    <strong>Nick: {{ $user->u_nick }} </strong><br>
    <strong>Tipo: {{ $user->u_type }} </strong><br><br>

    <div class="panel panel-default">
        <strong> Motivos que maneja </strong>
        <div class="panel-body">

            @foreach($reason_user as $var)
            ✓ {{$var->r_name}}<br>

            @endforeach
        </div>
    </div>
</div>

</div>





@stop