@extends ('layout')

@section ('title') Usuarios @stop

@section ('content')

@section ('pageheader') Nuevo Usuario @stop


<div class="row">
    <div class="col-md-12">



        {{ Form::open(array('url' => 'users/store', 'method' => 'POST', 'class' => 'form-horizontal'))   }}
        <fieldset>





            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Nombre Completo:</label>
                <div class="col-sm-10 ">


                    {{ Form::text('u_name', Input::old('u_name'), array('class' => 'form-control')) }}
                    @if($errors->has('u_name'))    
                    <p class="text-danger">{{ $errors->first('u_name') }}</p>
                    @endif
                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Nick:</label>
                <div class="col-sm-10 ">


                    {{ Form::text('u_nick', Input::old('u_nick'), array('class' => 'form-control')) }}
                    @if($errors->has('u_nick'))    
                    <p class="text-danger">{{ $errors->first('u_nick') }}</p>
                    @endif
                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Email:</label>
                <div class="col-sm-10 ">


                    {{ Form::email('u_email', Input::old('u_email'), array('class' => 'form-control')) }}
                    @if($errors->has('u_email'))    
                    <p class="text-danger">{{ $errors->first('u_email') }}</p>
                    @endif
                </div>
            </div>




            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Tipo:</label>
                <div class="col-sm-10 ">


                    {{ Form::select('u_type', [
   'Administrador' => 'Administrador',
   'Manager' => 'Manager'],null, ['class' => 'selectpicker form-control'])  }}



                    
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Departamento *</label>
                <div class="col-sm-10">



                    {{ Form::select('u_department_id', $departments, null,['class' => 'selectpicker form-control']) }}



                </div>
            </div>



            <div class="panel panel-default col-md-10 col-md-offset-2 ">
                <div class="panel-heading"><h3 class="panel-title">Motivos que maneja</h3></div>
                <div class="panel-body">
                    <div class="checkbox">



                        @foreach ($reasons as $var)

                        <label>{{ Form::checkbox('reason[]', $var->r_id) }} {{$var->r_name}}</label>
                        @endforeach
                          @if($errors->has('reason'))    
                    <p class="text-danger">{{ $errors->first('reason') }}</p>
                    @endif

                    </div>

                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>
                        
                    </div>
                </div>
            </div>



        </fieldset>
        {{ Form::close() }}
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->











@stop