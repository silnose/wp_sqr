@extends ('layout')

@section ('title') Editar Queja Reclamo Sugerencia @stop

@section ('content')

@section ('pageheader') Nueva Queja Reclamo Sugerencia @stop


<div class="row">
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Inicio</a></li>
        <li><a href="{{ URL::to('/complaints') }}">Reclamos</a></li>
        <li><a href="#">Editar Reclamo</a></li>

    </ol>

    <div class="col-md-12">

        {{ Form::model($complaint, array('url' => 'complaints/update/'.$complaint->c_id, 'method' => 'PUT', 'class' => 'form-horizontal'))   }}
        <fieldset>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Tipo *</label>
                <div class="col-sm-10">
                    {{ Form::select('c_type', ['queja' => 'Queja','reclamo' => 'Reclamo','sugerencia' => 'Sugerencia'],null, ['class' => 'form-control'] ) }}

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Apellido del Cliente * </label>
                <div class="col-sm-4">

                    {{ Form::text('c_lastname', Input::old('c_lastname'), array('class' => 'form-control')) }}
                    @if($errors->has('c_lastname'))    
                    <p class="text-danger">{{ $errors->first('c_lastname') }}</p>
                    @endif
                </div>

                <label class="col-sm-2 control-label" for="textinput">Nombre del Cliente *</label>
                <div class="col-sm-4 ">

                    {{ Form::text('c_name', Input::old('c_name'), array('class' => 'form-control')) }}
                    @if($errors->has('c_name'))    
                    <p class="text-danger">{{ $errors->first('c_name') }}</p>
                    @endif
                </div>
                 </div>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">DNI *</label>
                <div class="col-sm-10">
                    {{ Form::text('c_dni', Input::old('c_dni'), array('class' => 'form-control')) }}
                    @if($errors->has('c_dni'))    
                    <p class="text-danger">{{ $errors->first('c_dni') }}</p>
                    @endif
                </div>
            </div>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Teléfono Fijo</label>
                <div class="col-sm-10">
                    {{ Form::text('c_phone', Input::old('c_phone'), array('class' => 'form-control')) }}
                  

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Celular</label>
                <div class="col-sm-10">
                    {{ Form::text('c_cellphone', Input::old('c_cellphone'), array('class' => 'form-control')) }}
                    

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">E-Mail</label>
                <div class="col-sm-10">

                    {{ Form::email('c_email', Input::old('c_email'), array('class' => 'form-control')) }}
                    @if($errors->has('c_email'))    
                    <p class="text-danger">{{ $errors->first('c_email') }}</p>
                    @endif
                </div>
            </div>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Lugar de Trabajo</label>
                <div class="col-sm-10">
                    {{ Form::text('c_work', Input::old('c_work'), array('class' => 'form-control')) }}
                   

                </div>
            </div>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Teléfono del Trabajo</label>
                <div class="col-sm-10">
                    {{ Form::text('c_workphone', Input::old('c_workphone'), array('class' => 'form-control')) }}
                    

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Estado *</label>
                <div class="col-sm-10">
                    {{ Form::select('c_state', ['Pendiente' => 'Pendiente','En Proceso' => 'En Proceso','Terminada' => 'Terminada'],null, ['class' => 'form-control'] ) }}

                </div>
            </div>

                @if(Auth::user()->u_type == 'Administrador'):
            
             <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Plazo de Resolución</label>
                <div class="col-sm-10">
                    {{ Form::text('c_term', Input::old('c_term'), array('class' => 'form-control')) }}
                   

                </div>
            </div>
             
            @endif;

            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Motivo *</label>
                <div class="col-sm-10">



                    {{ Form::select('c_reason_id', $reason_id, $complaint->c_reason_id,['class' => 'form-control']) }}


                </div>
            </div>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Canal *</label>
                <div class="col-sm-10">



                    {{ Form::select('c_channel_id', $channels, $complaint->c_channel_id ,['class' => 'form-control']) }}



                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Detalle *</label>
                <div class="col-sm-10">
                    {{ Form::textarea('c_detail', Input::old('c_detail'), ['class' => 'form-control']) }}
                    @if($errors->has('c_detail'))    
                    <p class="text-danger">{{ $errors->first('c_detail') }}</p>
                    @endif
                </div>
            </div>

            
         
            
            
            {{ Form::hidden('c_user_id', Auth::user()->u_id) }}





            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>

                    </div>
                </div>
            </div>



        </fieldset>
        {{ Form::close() }}
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->











@stop
