@extends ('layout')

@section ('title') Queja Reclamo Sugerencia @stop

@section ('content')

@section ('pageheader') Nueva Queja Reclamo Sugerencia @stop


<div class="row">

    <ol class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Inicio</a></li>
        <li><a href="{{ URL::to('/complaints') }}">Reclamos</a></li>
        <li><a href=".">Nuevo Reclamo</a></li>

    </ol>

    <div class="col-md-12 center-block">




        {{ Form::open(array('url' => 'complaints/store', 'method' => 'POST', 'class' => 'form-horizontal'))   }}
        <fieldset>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Tipo *</label>
                <div class="col-sm-10">
                    {{ Form::select('c_type', ['queja' => 'Queja','reclamo' => 'Reclamo','sugerencia' => 'Sugerencia'],null, ['class' => 'selectpicker form-control'] ) }}

                </div>
            </div>


             <!-- Text input-->
            <div class="form-group"> 
                <label class="col-sm-2 control-label" for="textinput">DNI *</label>
                <div class="col-sm-10">
                    <div class="input-group">
                    {{ Form::text('c_dni', Input::old('c_dni'), array('class' => 'form-control', 'id' => 'c_dni', 'maxlength'=>'8')) }}
                    <span class="input-group-btn">
        <button class="btn btn-warning" type="button" id="search">Buscar</button>
                    </span>
                    </div>
                    @if($errors->has('c_dni'))    
                    <p class="text-danger">{{ $errors->first('c_dni') }}</p>
                    @endif
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Apellido del Cliente * </label>
                <div class="col-sm-4">

                    {{ Form::text('c_lastname', Input::old('c_lastname'), array('class' => 'form-control', 'id'=>'c_lastname')) }}
                    @if($errors->has('c_lastname'))    
                    <p class="text-danger">{{ $errors->first('c_lastname') }}</p>
                    @endif
                </div>

                <label class="col-sm-2 control-label" for="textinput">Nombre del Cliente *</label>
                <div class="col-sm-4 ">

                    {{ Form::text('c_name', Input::old('c_name'), array('class' => 'form-control', 'id'=>'c_name')) }}
                    @if($errors->has('c_name'))    
                    <p class="text-danger">{{ $errors->first('c_name') }}</p>
                    @endif
                </div>



            </div>





           


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Teléfono Fijo</label>
                <div class="col-sm-10">

                    {{ Form::text('c_phone', Input::old('c_phone'), array('class' => 'form-control', 'id'=>'c_phone' )) }}

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Celular</label>
                <div class="col-sm-10">
                    {{ Form::text('c_cellphone', Input::old('c_cellphone'), array('class' => 'form-control', 'id'=>'c_cellphone')) }}

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">E-Mail</label>
                <div class="col-sm-10">

                    {{ Form::text('c_email', Input::old('c_email'), array('class' => 'form-control', 'id'=>'c_email')) }}
                    @if($errors->has('c_email'))    
                    <p class="text-danger">{{ $errors->first('c_email') }}</p>
                    @endif
                </div>
            </div>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Lugar de Trabajo</label>
                <div class="col-sm-10">
                    {{ Form::text('c_work', Input::old('c_work'), array('class' => 'form-control', 'id'=>'c_work')) }}

                </div>
            </div>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Teléfono del Trabajo</label>
                <div class="col-sm-10">
                    {{ Form::text('c_phonework', Input::old('c_phonework'), array('class' => 'form-control', 'id'=>'c_phonework')) }}

                </div>
            </div>



            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Documentación*</label>
                <div class="col-sm-10">
                    {{ Form::select('c_doc', ['si' => 'Si','no' => 'No'],null, ['class' => 'selectpicker form-control'] ) }}

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Motivo *</label>
                <div class="col-sm-10">
                    <select name="c_reason_id" class="selectpicker form-control">

                        @foreach($reasons as $var) 
                        <option value="{{ $var->r_id}}">{{ $var->r_name}}

                        </option>

                        @endforeach

                    </select>

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Canal *</label>
                <div class="col-sm-10">



                    {{ Form::select('c_channel_id', $channels, null,['class' => 'selectpicker form-control']) }}



                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Detalle *</label>
                <div class="col-sm-10">
                    {{ Form::textarea('c_detail', Input::old('c_detail'), ['class' => 'form-control', 'id' =>'c_detail']) }}

                </div>
            </div>




            {{ Form::hidden('c_state', "Pendiente") }}
            {{ Form::hidden('date', date("Y")) }}
            {{ Form::hidden('c_user_id', Auth::user()->u_id) }}
            




            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>

                    </div>
                </div>
            </div>



        </fieldset>
        {{ Form::close() }}
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->











@stop