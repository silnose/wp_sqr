<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html lang="es">
<head>
<meta charset="utf-8">

    

<style type="text/css">
table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #a9a9a9;border-collapse: collapse;}
table.tftable th {font-size:12px;background-color:#b8b8b8;border-width: 1px;padding: 8px;border-style: solid;border-color: #a9a9a9;text-align:left;}
table.tftable tr {background-color:#ffffff;}
table.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #a9a9a9;}
h2 { text-align: center}

</style>


     <title> <?php echo $complaint->c_id.' - '.strtoupper($complaint->c_lastname).', '.$complaint->c_name  ?> </title>
     <!-- Generated from http://html-generator.weebly.com -->
     

</head>

<body>
<small> Departamento de <?php echo utf8_decode($department->d_name)?></small>
     
<h2>Seguimiento</h2>

<table id="tfhover" class="tftable" border="1">
    <thead>
        <tr>
            <th>Expediente N°</th>
            <th><?php echo utf8_decode('Motívo') ?></th>
            <th><?php echo utf8_decode('Certificado Número') ?></th>
            <th>F.Ingreso</th>
            <th>F.P.P</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td><?php echo $complaint->c_id ?></td>
            <td><?php echo utf8_decode($reason->r_name) ?></td>
            <td> </td>
            <td><?php echo $complaint->created_at ?></td>
            <td><?php echo $complaint->c_phone ?> </td>
        </tr>
        <tr>
            
            <td colspan="3"> Cliente: <?php echo utf8_decode(strtoupper($complaint->c_lastname).', '.$complaint->c_name )?></td>
            <td>D.N.I: <?php echo $complaint->c_dni ?> </td>
            <td>Email: <?php echo $complaint->c_email ?></td>
        </tr>

        

        <tr>
            <td colspan="5">
                <div class="panel-body">
                    <?php echo utf8_decode($complaint->c_detail) ?>
                </div>



            </td>
        </tr>
    </tbody>
</table>
</body>

</html>
