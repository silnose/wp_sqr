@extends ('layout')

@section ('title') Quejas Reclamos y Sugerencias @stop

@section ('content')

@section ('pageheader') Quejas Reclamos y Sugerencias @stop

<a class="btn btn-success" href="{{ URL('/complaints/create' ) }}" >Nueva Queja Reclamo</a>
<br><br>

<ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('#') }}">Reclamos</a></li>


</ol>

@if (Session::has('message'))
<div class="alert alert-warning">{{ Session::get('message') }}</div>
@endif


<table class="table table-responsive">
    <tr>
        <th>N°</th>
        <th>Apellido y Nombre</th>
        <th>Motivo</th>
        <th>Estado</th>
        <th>Departamentos</th>
        <th>Proceso</th>
        <th>Acciones</th>


    </tr>
    @foreach ($complaints as $complaint)
    <?php if ($complaint->c_state != 'Terminado' && $complaint->c_term == 1): ?>
        <tr class="alert alert-danger">
        <?php elseif ($complaint->c_state == 'Pendiente'): ?>
        <tr class="alert alert-warning">
        <?php elseif ($complaint->c_state == 'En Proceso'): ?>
        <tr class="alert alert-info">

        <?php elseif ($complaint->c_state == 'Terminada'): ?>
        <tr class="alert alert-success">

        <?php endif; ?>

        <td>{{ $complaint->c_id }}</td>
        <td>{{ $complaint->c_lastname.", ".$complaint->c_name }}</td>
        <td>{{ $complaint->r_name }}</td>
        <td>{{ $complaint->c_state }}</td>
        <td>{{ $complaint->d_name }}</td>
        <td>{{ $complaint->c_final_date }}</td>
        <td><a href="{{ URL('/complaints/show', $complaint->c_id ) }}" title="ver" > <span class="btn btn-success glyphicon glyphicon-eye-open"></span></a>
            <a href="{{ URL('/complaints/edit', $complaint->c_id ) }}" title="editar"><span class="btn btn-warning glyphicon glyphicon-edit"></span></a>
            @if(Auth::user()->u_type == 'Administrador')
            <a href="{{ URL('/complaints/destroy', $complaint->c_id ) }}" title="eliminar" ><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
            @endif
            <a href="{{ URL('/complaints/pdf', $complaint->c_id ) }}" title="generar pdf" target="_blank" > <span class="btn btn-info glyphicon glyphicon-file"></span></a>
        </td>


    </tr>
    @endforeach
</table>

{{ $complaints->links(); }}




@stop