@extends ('layout')

@section ('title') Queja Reclamos Sugerencia @stop

@section ('content')

@section ('pageheader') Seguimiento <small>Departamento de {{ $department->d_name}}</small>  @stop


<div class="row">

    <ol class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Inicio</a></li>
        <li><a href="{{ URL::to('/complaints') }}">Reclamos</a></li>
        <li><a href="{{ URL('/vista') }}">vista</a></li>
        <li><a href="#">Ver Reclamo</a></li>

    </ol>

      <div class="jumbotron text-center">
            <h2>{{ $complaint->c_id.' - '.$complaint->c_lastname.', '.$complaint->c_name }}</h2>
            <h2> {{ $reason->r_name }}</h2>
            <strong>DNI°: {{ $complaint->c_dni }}</strong><br>
            <strong>Estado: {{ $complaint->c_state }}</strong><br>
            <strong>Fecha Limite: {{ $complaint->c_final_date }}</strong><br>
            <strong>Dias Restantes: <?php 
            if($dias < 0) {
                $dias = 0;
                echo $dias; 
                
                } 
                else{
                    echo $dias;}
                    ?></strong><br>
            <strong>Canal: {{$channel}}</strong><br>
            <strong>Tel: {{$complaint->c_phone }}</strong><br>
            <strong>Cel: {{$complaint->c_cellphone }}</strong><br>
            <strong>Trabaja en: {{$complaint->c_work  }}</strong><br>
            <strong>Tel del Trabajo: {{$complaint->c_workphone }}</strong><br><br>
            <div class="panel panel-default">
    
    <div class="panel-body">
        {{ $complaint->c_detail }}
    </div>
</div>
        </div>   
    
    

    
    
	

        
 
</div>













@stop