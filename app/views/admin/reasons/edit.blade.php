@extends ('layout')

@section ('title') Motivos @stop

@section ('content')

@section ('pageheader') Editar Motivo @stop


<div class="row">
        <ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('/reasons') }}">Motivos</a></li>
    <li><a href="{{ URL::to('#') }}">Editar Motivo</a></li>

</ol>
    
    
    <div class="col-md-12">

        {{ Form::model($reason, array('url' => 'reasons/update/'.$reason->r_id, 'method' => 'PUT', 'class' => 'form-horizontal'))   }}
        <fieldset>


            
          
   
            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Motivo</label>
                <div class="col-sm-10 ">
                    {{ Form::input('text', 'r_name', null, ['class' => 'form-control']) }}

                </div>
            </div>
          

            
            <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Plazo de Resolucion</label>
                <div class="col-sm-10">
                    {{ Form::input('text','r_term', null, ['class' => 'form-control']) }}
                </div>
            </div>

            
                    <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Departamento *</label>
                <div class="col-sm-10">
               
                    
                    
                    {{ Form::select('r_department_id', $department_id, $reason->department_id,['class' => 'selectpicker form-control']) }}
                    
                    
                    
                </div>
            </div>
                    

                    <!-- Text input-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="textinput">Detalle</label>
                <div class="col-sm-10">
                    {{ Form::textarea('r_detail', Input::old('r_detail'), ['class' => 'selectpicker form-control']) }}
                    @if($errors->has('r_detail'))    
                    <p class="text-danger">{{ $errors->first('r_detail') }}</p>
                    @endif
                </div>
            </div>
                    
           




           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>
                        
                    </div>
                </div>
            </div>

         
            
        </fieldset>
        {{ Form::close() }}
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->











@stop
