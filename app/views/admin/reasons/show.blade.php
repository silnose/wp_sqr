@extends ('layout')

@section ('title') Motivos @stop

@section ('content')

@section ('pageheader') Motivos @stop

<div class="row">
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Inicio</a></li>
        <li><a href="{{ URL::to('/reasons') }}">Motivos</a></li>
        <li><a href="{{ URL::to('#') }}">Ver Motivo</a></li>

    </ol>
    <div class="jumbotron text-center">

        <h2>{{ $reason->r_name }} </h2>
        <strong>Plazo de Resolucion: {{ $reason->r_term }} dia/s</strong><br>
        <strong>Departamento: {{ $department->d_name }} </strong><br><br>
        <div class="panel panel-default">

            <div class="panel-body">
                {{ $reason->r_detail }}
            </div>
        </div>
    </div>  








</div>      





@stop