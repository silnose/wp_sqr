@extends ('layout')

@section ('title') Motivos @stop

@section ('content')

@section ('pageheader') Motivos @stop

<a class="btn btn-success" href="{{ URL('/reasons/create' ) }}" >Nuevo Motivo</a>
<br><br>

<ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('#') }}">Motivos</a></li>
    

</ol>

@if (Session::has('message'))
<div class="alert alert-warning">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped">
    <tr>
        <th>N°</th>
        <th>Motivo</th>
        <th>Detalle</th>
        <th>Plazo</th>
        <th>Acciones</th>
     
         
       
    </tr>
    @foreach ($reasons as $reason)
    <tr>
        <td>{{ $reason->r_id }}</td>
        <td>{{ $reason->r_name }}</td>
        <td>{{ $reason->r_detail }}</td>
        <td>{{ $reason->r_term }} dias</td>
        
             <td><a href="{{ URL('/reasons/show', $reason->r_id ) }}" title="ver" > <span class="btn btn-success glyphicon glyphicon-eye-open"></span></a>
            <a href="{{ URL('/reasons/edit', $reason->r_id ) }}" title="editar"><span class="btn btn-warning glyphicon glyphicon-edit"></span></a>
            <a href="{{ URL('/reasons/destroy', $reason->r_id ) }}" title="eliminar" ><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
        </td>
        
    </tr>
    @endforeach
  </table>
  
 {{ $reasons->links() }}  


  

@stop