@extends ('layout')

@section ('title') Canales de Comunicación @stop

@section ('content')

<div style="text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;" class="hero-unit">
    <h1> BIENVENIDO/A, {{ strtoupper(Auth::user()->u_name)}} </h1>
    <hr>
  
    
    </div>

    
<div class="container">
    <br>
	<div class="row">
        <div class="col-md-6">
            <a href="{{ URL('/complaints/create' ) }}" class="btn btn-warning btn-lg btn-block btn-huge"><span class="glyphicon glyphicon-flash"></span> Nuevo Reclamo</a>
        </div>
        <div class="col-md-6">
            <a href="{{ URL('/complaints' ) }}" class="btn btn-warning btn-lg btn-block btn-huge"><span class="glyphicon glyphicon-pushpin"></span> Tus Reclamos </a>
        </div>
     
            
        
	</div>



</div>





@stop