<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">



     <title>@yield('title', 'Mañana Seguros')</title>

    <!-- Bootstrap -->
    {{ HTML::style('assets/css/bootstrap.min.css', array('media' => 'screen')) }}
    {{ HTML::style('assets/css/bootstrap-select.min.css', array('media' => 'screen')) }}

    <!-- MetisMenu CSS -->
    {{ HTML::style('assets/css/plugins/metisMenu/metisMenu.min.css') }}
     
 

   
   

    <!-- Custom CSS -->
    {{ HTML::style('assets/css/sb-admin-2.css') }}
    
    
   

 

    <!-- Custom Fonts -->
    {{ HTML::style('assets/font-awesome-4.1.0/css/font-awesome.min.css', array('type' => 'text/css', 'rel'=>'stylesheet' )) }}
    


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
.btn-huge{
    padding-top:20px;
    padding-bottom:20px;
}   
        
    </style>
    
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
            
            <a class="navbar-brand" href="#" style="margin-bottom: 15px">
                <img src="{{ URL('images/logo.png') }}" alt="" >
                </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
            
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>&nbsp;&nbsp; {{ Auth::user()->u_nick }}
                    </a>
                    <ul class="dropdown-menu dropdown-user"> 
                        <li>
                            <a href="{{ URL('/users/edit', Auth::user()->u_id ) }}" ><i class="fa fa-gear fa-fw"></i> Configuracion</a>
                        </li>
                       
                        <li class="divider"></li>
                        <li><a href="{{ URL('change_password') }}"><i class="fa fa-lock fa-fw"></i> Cambiar Contraseña</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li><a href="{{ URL('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <form method="get" action="{{ URL('/complaints/search') }}">
                                      <input type="text" name="q" class="form-control" placeholder="DNI | APELLIDO" required>
                                
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                                  </form>  
                            </div>
                            
                            <!-- /input-group -->
                        </li>
                       
                        
                        <li>
                            <a href="{{ URL('/' ) }}"><i class="fa fa-home fa-fw"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-comment fa-fw"></i> Reclamos <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ URL('/complaints/create' ) }}" >Nuevo Reclamo</a>
                                </li>
                                
                                <li>
                                    <a href="{{ URL('/complaints' ) }}" >Reclamos</a>
                                </li>
                            
                              
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @if(Auth::user()->u_type == 'Administrador')
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Configuración<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ URL('/channels' ) }}" >Canales de Comunicacíon</a>
                                </li>
                                
                                <li>
                                    <a href="{{ URL('/departments' ) }}" >Departamentos</a>
                                </li>
                                
                                <li>
                                    <a href="{{ URL('/users' ) }}" >Usuarios</a>
                                </li>
                                
                                <li>
                                    <a href="{{ URL('/reasons' ) }}" >Motivos</a>
                                </li>
                              
                                
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        @endif
                        
                        
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Reportes</a>
                        </li>
                        
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('pageheader')</h1>
                @yield('content')
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


   {{ Form::hidden('base_url', URL::to('/')) }}
    
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {{ HTML::script('assets/js/jquery-1.11.0.js') }}
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {{ HTML::script('assets/js/bootstrap.min.js') }}
    {{ HTML::script('assets/js/bootstrap-select.js') }}

    <!-- Metis Menu Plugin JavaScript -->
    {{ HTML::script('assets/js/plugins/metisMenu/metisMenu.min.js') }}
   
    <!-- Custom Theme JavaScript -->
    {{ HTML::script('assets/js/sb-admin-2.js') }}
    

@if (isset($js))

@foreach ($js as $var)


{{ HTML::script($var) }}

@endforeach

@endif
</body>

</html>
