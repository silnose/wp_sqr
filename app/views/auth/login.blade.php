<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

     <title>@yield('title', 'Mañana Seguros')</title>

    <!-- Bootstrap -->
    {{ HTML::style('assets/css/bootstrap.min.css', array('media' => 'screen')) }}
    
    <style>
        
        .form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 20px;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    
    margin: 0 auto 10px;
    display: block;
   
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
}
    </style>
    
</head>

<body>
<div class="container">
    
    <div class="row">
       

        <div class="col-sm-6 col-md-4 col-md-offset-4">
             @if (Session::has('message'))
<div class="alert alert-danger">{{ Session::get('message') }}</div>
@endif

            <div class="account-wall">
                <img class="profile-img" src="{{ URL('images/logo.png') }}" alt="">
               
                {{ Form::open(array('url' => 'login', 'class'=>'form-signin')) }}
                
                
                {{ Form::text('u_nick',null, array('class' => 'form-control', 'placeholder'=>'Nombre de Usuario' )) }}
                
                {{ Form::password('u_password', array('class' => 'form-control', 'placeholder'=>'Contraseña')) }}
                
                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Ingresar</button>
                
                
                {{ Form::close() }}
            </div>
           
        </div>
    </div>
</div>

 

 
 


       <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {{ HTML::script('assets/js/jquery-1.11.0.js') }}
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {{ HTML::script('assets/js/bootstrap.min.js') }}
    
</body>
</html>