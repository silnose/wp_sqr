@extends ('layout')

@section ('title') Cambiar Contraseña @stop

@section ('content')

@section ('pageheader') Cambiar Contraseña @stop

<ol class="breadcrumb">
    <li><a href="{{ URL::to('/') }}">Inicio</a></li>
    <li><a href="{{ URL::to('#') }}">Cambiar Contraseña</a></li>  
</ol>

{{ Form::open(array('url' => 'change_password', 'method' => 'POST', 'class' => 'form-horizontal'))   }}
<fieldset>




    <!-- Text input-->
    <div class="form-group">
        <label class="col-sm-2 control-label" for="textinput">Contraseña Antigua</label>
        <div class="col-sm-10 ">
            {{ Form::password('password_old', Input::old('password_old'), array('class' => 'form-control')) }}
            @if($errors->has('password_old'))    
            <p class="text-danger">{{ $errors->first('password_old') }}</p>
            @endif
        </div>

    </div>

    <!-- Text input-->
    <div class="form-group">
        <label class="col-sm-2 control-label" for="textinput">Contraseña Nueva</label>
        <div class="col-sm-10 ">


            {{ Form::password('password', Input::old('password'), array('class' => 'form-control')) }}
            @if($errors->has('password'))    
            <p class="text-danger">{{ $errors->first('password') }}</p>
            @endif
        </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
        <label class="col-sm-2 control-label" for="textinput">Contraseña Nueva</label>
        <div class="col-sm-10 ">


            {{ Form::password('password_again', Input::old('password_again'), array('class' => 'form-control')) }}
            @if($errors->has('password_again'))    
            <p class="text-danger">{{ $errors->first('password_again') }}</p>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="pull-right">
                <button type="submit" class="btn btn-success">Guardar</button>


            </div>
        </div>
    </div>

    {{Form::close() }}


    @stop