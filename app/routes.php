<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */





// esta sera la ruta principal de nuestra aplicación
// aquí va a estar el formulario para registrase y para inicio de sesión
// esta ruta debe ser publica y por lo tanto no debe llegar el filtro auth
Route::get('login', function() {
    return View::make('auth/login');
});


// esta ruta servirá para iniciar la sesión por medio del correo y la clave
// para esto utilizamos la función estática attemp de la clase Auth
// esta función recibe como parámetro un arreglo con el correo y la clave
Route::post('login', function() {

    $userdata = array(
        'u_nick' => Input::get('u_nick'),
        'password' => Input::get('u_password')
    );
    if (Auth::attempt($userdata)):
        if (Auth::user()->u_type == 'Administrador'):
            return Redirect::to('/');
        elseif (Auth::user()->u_type == 'Manager'): {
                return Redirect::to('users');
            }
        endif;

    else:
        Session::flash('message', 'Usuario o Contraseña Incorrectos');
        return Redirect::to('login');
    endif;


    // la función attempt se encarga automáticamente se hacer la encriptación de la clave para ser comparada con la que esta en la base de datos.
    //print_r($userdata);
});

// Por ultimo crearemos un grupo con el filtro auth.
// Para todas estas rutas el usuario debe haber iniciado sesión.
// En caso de que se intente entrar y el usuario haya iniciado session
// entonces sera redirigido a la ruta login
Route::group(array('before' => 'auth'), function() {



    Route::get('/', function() {
        return View::make('index');
    });


    Route::get('change_password', function() {
    return View::make('auth/change_password');
});

    Route::post('change_password', function() {
    
         $rules = array(
            'password_old' => 'required ',
            'password' => 'required | min:6 ',
            'password_again' => 'required | same:password',
          
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('change_password')
                            ->withErrors($validator);
        }
        else{
            
            $user = User::find(Auth::user()->u_id);
            $password_old = Input::get('password_old');
            $password = Input::get('password');
           
            
            if(Hash::check($password_old, $user->getAuthPassword())){
               $user->u_password = Hash::make($password);
                
                if($user->save()){
                      Session::flash('message', 'Contraseña Editada con Exito');
        return Redirect::to('users');
                    
                }
                
            }
            
            else
            {
                
                 Session::flash('message', 'No se pudo cambiar');
        return Redirect::to('users');
                    
                
            }
        }

});
    Route::get('complaints/search', 'ComplaintController@search');
    Route::get('complaints/json_client', 'ComplaintController@json_client');
    Route::get('complaints/pdf/{id}', 'ComplaintController@pdf');
    Route::get('complaints/', 'ComplaintController@index');
    Route::get('complaints/show/{id}', 'ComplaintController@show');
    Route::post('complaints/store', 'ComplaintController@store');
    Route::get('complaints/edit/{id}', 'ComplaintController@edit');
    Route::put('complaints/update/{id}', 'ComplaintController@update');
    Route::get('complaints/destroy/{id}', 'ComplaintController@destroy');
    Route::get('complaints/create', 'ComplaintController@create');


    Route::get('reasons', 'ReasonController@index');
    Route::get('reasons/show/{id}', 'ReasonController@show');
    Route::post('reasons/store', 'ReasonController@store');
    Route::get('reasons/edit/{id}', 'ReasonController@edit');
    Route::put('reasons/update/{id}', 'ReasonController@update');
    Route::get('reasons/destroy/{id}', 'ReasonController@destroy');
    Route::get('reasons/create', 'ReasonController@create');


    Route::get('departments', 'DepartmentController@index');
    Route::get('departments/show/{id}', 'DepartmentController@show');
    Route::post('departments/store', 'DepartmentController@store');
    Route::get('departments/edit/{id}', 'DepartmentController@edit');
    Route::put('departments/update/{id}', 'DepartmentController@update');
    Route::get('departments/destroy/{id}', 'DepartmentController@destroy');
    Route::get('departments/create', 'DepartmentController@create');


    Route::get('channels', 'ChannelController@index');
    Route::get('channels/show/{id}', 'ChannelController@show');
    Route::post('channels/store', 'ChannelController@store');
    Route::get('channels/edit/{id}', 'ChannelController@edit');
    Route::put('channels/update/{id}', 'ChannelController@update');
    Route::get('channels/destroy/{id}', 'ChannelController@destroy');
    Route::get('channels/create', 'ChannelController@create');


    Route::get('users', 'UserController@index');
    Route::get('users/show/{id}', 'UserController@show');
    Route::post('users/store', 'UserController@store');
    Route::get('users/edit/{id}', 'UserController@edit');
    Route::put('users/update/{id}', 'UserController@update');
    Route::get('users/destroy/{id}', 'UserController@destroy');
    Route::get('users/create', 'UserController@create');
});

Route::get('logout', function() {

    Auth::logout();
    return Redirect::to('login');
});

